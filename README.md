# dot-emacs

A new emacs config I am working on.

## Motivation

This is the 3rd time I'm rewriting my `Emacs` configs.  The previous two were
more of a trial phase for me as I transitioned from `ViM`.

I was very bottered by the **massive** load time compared to `ViM`.  So I looked
around the internet and found
[doom-emacs](https://github.com/hlissner/doom-emacs).

This was a pretty big revelation to me, as I had never really tried reducing the
startup time.  And well, this is my take on it.

## Acknowledgements

I took some inspiration from the previously mentioned
[doom-emacs](https://github.com/hlissner/doom-emacs)
and this great [article](https://blog.d46.us/advanced-emacs-startup/) I used as
reference.
