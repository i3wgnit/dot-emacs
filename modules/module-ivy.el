;;; module-ivy.el -*- lexical-binding: t; -*-

(use-package ivy
  :delight
  :defer 1
  :init
  (twl|load-after-hook pre-command-hook ivy)
  (setq ivy-re-builders-alist '((t . ivy--regex-ignore-order)))
  :config
  (setq
   ivy-height 15
   ivy-wrap t
   ivy-fixed-height-minibuffer t

   ivy-format-function #'ivy-format-function-line
   ivy-initial-inputs-alist nil
   ivy-magic-slash-non-match-action nil
   ivy-on-del-error-function nil
   ivy-use-selectable-prompt t
   ivy-use-virtual-buffers nil
   ivy-virtual-abbreviate 'full)

  (ivy-mode +1))

(use-package swiper
  :general
  (define-leader-key
    "/" (list #'swiper :jump t)
    "?" (list #'swiper-backward :jump t))
  :config
  (add-to-list 'ivy-re-builders-alist '(swiper . ivy--regex-plus)))

(use-package counsel
  :general
  (define-leader-key
    "f" #'counsel-fzf)
  ([remap swiper] #'counsel-grep-or-swiper
   [remap swiper-backward] #'counsel-grep-or-swiper-backward
   [remap find-file] #'counsel-find-file
   [remap execute-extended-command] #'counsel-M-x
   [remap recentf-open-files] #'counsel-recentf)
  :init
  (setq amx-save-file (concat twl-local-dir "amx-items"))
  :config
  (add-to-list 'ivy-re-builders-alist '(counsel-ag . ivy--regex-plus))
  (add-to-list 'ivy-re-builders-alist '(counsel-rg . ivy--regex-plus))
  (add-to-list 'ivy-re-builders-alist '(counsel-grep . ivy--regex-plus)))

(use-package flx
  :init
  (setf (alist-get 't ivy-re-builders-alist) #'ivy--regex-fuzzy)
  (setq ivy-flx-limit 9999))

(provide 'module-ivy)
