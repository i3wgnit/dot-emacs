;;; module-which-key.el -*- lexical-binding: t; -*-

(use-package which-key
  :delight
  :defer 1
  :init
  (twl|load-after-hook pre-command-hook which-key)
  :config
  (setq which-key-sort-order #'which-key-prefix-then-key-order
        which-key-sort-uppercase-first nil
        which-key-add-column-padding 1
        which-key-max-display-columns nil
        which-key-min-display-lines 4
        which-key-side-window-slot -10)
  (set-face-attribute 'which-key-local-map-description-face nil :weight 'bold)
  (which-key-setup-side-window-bottom)

  (twl|setq-hook which-key-init-buffer-hook
                 line-spacing 3)

  (which-key-add-key-based-replacements twl-leader "<leader>")
  (which-key-add-key-based-replacements twl-localleader "<localleader>")

  (which-key-mode +1))

(provide 'module-which-key)
