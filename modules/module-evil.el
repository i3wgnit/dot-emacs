;;; module-evil.el -*- lexical-binding: t; -*-

(use-package evil
  :demand
  :init
  (setq
   ;; evil-want-integration nil
   evil-want-keybinding nil

   evil-want-C-i-jump t
   evil-want-C-u-scroll t
   evil-want-C-w-delete t
   evil-want-Y-yank-to-eol nil)
  :config
  (setq
   evil-indent-convert-tabs t
   evil-insert-skip-empty-lines t
   evil-want-visual-char-semi-exclusive t

   evil-ex-search-vim-style-regexp t
   evil-ex-substitute-global t
   evil-magic t
   evil-search-module 'evil-search

   evil-split-window-below t
   evil-vsplit-window-right t

   evil-emacs-state-modes nil

   evil-echo-state t
   evil-mode-line-format nil


   evil-insert-state-cursor 'bar
   evil-normal-state-cursor 'box
   evil-visual-state-cursor 'hollow)

  (defun twl+evil/disable-highlights ()
    (when (evil-ex-hl-active-p 'evil-ex-search)
      (evil-ex-nohighlight)))
  (add-hook 'twl-escape-hook #'twl+evil/disable-highlights)
  (advice-add #'evil-force-normal-state :after #'twl/escape)
  (twl/advice-add '(evil-ex-search-word-backward
                    evil-ex-search-word-forward
                    evil-search-word-backward
                    evil-search-word-forward
                    evil-search-next
                    evil-search-previous)
                  :after #'recenter)

  (evil-mode +1))

;;
;;; Keybinds

(general-unbind :states 'motion
  "K")

(define-leader-key
  "w" (list #'save-buffer :repeat 'abort)
  "q" #'kill-current-buffer
  "r" #'revert-buffer

  "b" #'ibuffer
  "e" #'find-file
  "h" #'dired-jump
  "x" #'execute-extended-command)

;;
;;; Packages

(use-package undo-tree
  :delight)

(use-package evil-lion
  :after evil
  :general
  (:states '(normal visual)
           "gl" #'evil-lion-left
           "gL" #'evil-lion-right))

(use-package evil-surround
  :after evil
  :general
  (:states 'operator
           "s" #'evil-surround-edit
           "S" #'evil-Surround-edit)
  (:states 'visual
           "S" #'evil-surround-region)
  :config
  (global-evil-surround-mode +1))

(use-package embrace
  :commands (embrace-add-pair embrace-add-pair-regexp)
  :hook
  ((LaTeX-mode . embrace-LaTeX-mode-hook)
   (org-mode . embrace-org-mode-hook)
   ((ruby-mode eeh-ruby-mode) . embrace-ruby-mode-hook)
   (emacs-lisp-mode . embrace-emacs-lisp-mode-hook)))

(use-package evil-embrace
  :after (embrace evil-surround)
  :demand
  :init
  (setq evil-embrace-show-help-p nil)
  :config
  (evil-embrace-enable-evil-surround-integration))

(use-package evil-exchange
  :after evil
  :general
  (:states '(normal visual)
           "gx" #'evil-exchange
           "gX" #'evil-exchange-cancel)
  :config
  (defun twl+evil/escape-exchange ()
    (when evil-exchange--position
      (evil-exchange--clean))
    t)
  (add-hook 'twl-escape-hook #'twl+evil/escape-exchange))

(use-package evil-visualstar
  :after evil
  :general
  (:states 'visual
           "*" #'evil-visualstar/begin-search-forward
           "#" #'evil-visualstar/begin-search-backward)
  :config
  (twl/advice-add '(evil-visualstar/begin-search-forward
                    evil-visualstar/begin-search-backward)
                  :after #'recenter))

;;
;;; Evil Collection

(use-package evil-collection
  :after evil
  :preface
  (setq evil-collection-setup-minibuffer nil))

(defvar twl+evil-evil-collection-disabled-list
  '(buff-menu
    comint
    custom
    elisp-mode
    help
    image
    info
    occur
    package-menu
    simple))
(defvar twl+evil-evil-collection-mode-list
  `(2048-game
    ag
    alchemist
    anaconda-mode
    arc-mode
    bookmark
    (buff-menu "buff-menu")
    calc
    calendar
    cider
    cmake-mode
    comint
    company
    compile
    custom
    cus-theme
    daemons
    deadgrep
    debbugs
    debug
    diff-mode
    dired
    disk-usage
    doc-view
    ebib
    edbi
    edebug
    ediff
    eglot
    elfeed
    elisp-mode
    elisp-refs
    elisp-slime-nav
    emms
    epa
    ert
    eshell
    eval-sexp-fu
    evil-mc
    eww
    flycheck
    flymake
    free-keys
    geiser
    ggtags
    git-timemachine
    go-mode
    grep
    guix
    hackernews
    helm
    help
    helpful
    hg-histedit
    hungry-delete
    ibuffer
    image
    image-dired
    image+
    imenu-list
    indium
    info
    ivy
    js2-mode
    leetcode
    log-edit
    log-view
    lsp-ui-imenu
    lua-mode
    kotlin-mode
    macrostep
    man
    magit
    magit-todos
    ,@(when evil-collection-setup-minibuffer '(minibuffer))
    monky
    mu4e
    mu4e-conversation
    neotree
    notmuch
    nov
    ;; occur is in replace.el which was built-in before Emacs 26.
    (occur ,(if EMACS26+ 'replace "replace"))
    omnisharp
    outline
    p4
    (package-menu package)
    pass
    (pdf pdf-view)
    popup
    proced
    process-menu
    prodigy
    profiler
    python
    quickrun
    racer
    realgud
    reftex
    restclient
    rjsx-mode
    robe
    ruby-mode
    rtags
    simple
    slime
    (term term ansi-term multi-term)
    tetris
    tide
    transmission
    typescript-mode
    vc-annotate
    vc-dir
    vc-git
    vdiff
    view
    vlf
    vterm
    w3m
    wdired
    wgrep
    which-key
    woman
    xref
    youtube-dl
    (ztree ztree-diff)))

(defun twl+evil/evil-collection-init (module &optional force-p)
  (when (or force-p
            (not (memq (or (car-safe module) module)
                       twl+evil-evil-collection-disabled-list)))
    (progn (let ((module-sym (or (car-safe module) module)))
             (twl/log "Initialized evil-collection-%s %s"
                      module-sym (if twl-init-time "" "(too early!)")))
           (evil-collection-init (list module)))))

(twl|after evil-collection
           (dolist (module '(comint custom help))
             (twl+evil/evil-collection-init module t)))

(defmacro twl+evil/evil-collection-init-on-load (req module)
  `(twl|add-transient-hook ,req
                           (twl+evil/evil-collection-init ,module t)))

(twl+evil/evil-collection-init-on-load
 'Buffer-menu-mode '(buff-menu "buf-menu"))
(twl+evil/evil-collection-init-on-load
 'image-mode 'image)
(twl+evil/evil-collection-init-on-load
 'info-mode 'info)
(twl+evil/evil-collection-init-on-load
 'emacs-lisp-mode 'elisp-mode)
(twl+evil/evil-collection-init-on-load
 'occur-mode `(occur ,(if EMACS26+ 'replace "replace")))
(twl+evil/evil-collection-init-on-load
 'package-menu-mode '(package-menu package))

(dolist (module twl+evil-evil-collection-mode-list)
  (dolist (req (or (cdr-safe module) (list module)))
    (with-eval-after-load req (twl+evil/evil-collection-init module))))

(provide 'module-evil)
