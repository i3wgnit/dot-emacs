;;; module-projectile.el -*- lexical-binding: t; -*-

(use-package projectile
  :delight
  :defer 1
  :general
  (define-leader-key
    "p" '(:keymap projectile-command-map))
  :config
  (setq
   projectile-completion-system 'ivy

   projectile-known-projects-file (concat twl-local-dir
                                          "projectile-bookmarks.eld")
   projectile-cache-file (concat twl-local-dir "projectile.cache")
   projectile-switch-project-action #'projectile-dired)

  (define-leader-key
    "p" '(:keymap projectile-command-map))

  (projectile-mode +1))

(provide 'module-projectile)
