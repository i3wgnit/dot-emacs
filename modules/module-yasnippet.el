;;; module-yasnippet.el -*- lexical-binding: t; -*-

(use-package yasnippet
  :delight (yas-minor-mode)
  :hook
  ((prog-mode text-mode conf-mode snippet-mode)
   . yas-minor-mode-on)
  :commands
  (yas-expand yas-expand-snippet yas-lookup-snippet
              yas-insert-snippet yas-new-snippet yas-visit-snippet-file)
  :init
  (twl|add-transient-hook 'yas-minor-mode-hook (yas-reload-all))
  :config
  (setq yas-verbosity (if twl-debug-mode 3 0)
        yas-also-auto-indent-first-line t
        yas-snippet-dirs (delete yas--default-user-snippets-dir
                                 yas-snippet-dirs))
  (add-to-list 'yas-snippet-dirs (concat twl-private-dir "snippets/")))

(provide 'module-yasnippet)
