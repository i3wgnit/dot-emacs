;;; module-tex.el -*- lexical-binding: t; -*-

(use-package tex
  :ensure auctex
  :commands TeX-source-correlate-mode
  :init
  (setq TeX-auto-private (concat twl-private-dir "auctex/auto/")
        TeX-auto-default (concat twl-private-dir "auctex/auto/")
        TeX-auto-global (concat twl-private-dir "auctex"))
  :config
  (setq TeX-auto-save t
        TeX-parse-self t
        TeX-electric-escape nil
        TeX-insert-braces nil)
  (add-to-list 'TeX-view-program-selection '(output-pdf "Skim")))

(use-package reftex
  :delight
  :after tex
  :demand
  :ensure nil
  :commands turn-on-reftex
  :init
  (setq reftex-plug-into-AUCTeX t))

(use-package latex
  :after tex
  :demand
  :ensure nil
  :hook ((LaTeX-mode . LaTeX-math-mode))
  :config
  (add-hook! LaTeX-mode
    '(turn-on-reftex TeX-source-correlate-mode))
  (setq
   LaTeX-fill-break-at-separators '(\\\( \\\[ \\\])
   LaTeX-math-list '(("v0" "varnothing")))

  (add-to-list 'LaTeX-indent-environment-list '("algorithmic"))
  (add-to-list 'LaTeX-indent-environment-list '("asy"))

  (defun twl+latex//fill-sentence (orig-fun from to &rest args)
    "Start each sentence on a new line."
    (let ((to-marker (set-marker (make-marker) to))
          tmp-end)
      (save-excursion
        (while (< from (marker-position to-marker))
          (forward-sentence)
          (when (looking-at " ") (backward-char))
          ;; might have gone beyond to-marker --- use whichever is smaller:
          (setq to (min (point) (marker-position to-marker)))
          (apply orig-fun from to args)
          (setq from (point))
          (save-excursion
            (back-to-indentation)
            (setq tmp-end (point)))
          (unless (or (bolp)
                      (eolp)
                      (texmathp)
                      (<= from tmp-end)
                      (looking-at ". *$"))
            (LaTeX-newline))))))
  (advice-add #'LaTeX-fill-region-as-paragraph
              :around #'twl+latex//fill-sentence))

(use-package auctex-latexmk
  :after latex
  :demand
  :init
  (setq-hook! LaTeX-mode
    TeX-command-default "LatexMk")
  :config
  (setq auctex-latexmk-inherit-TeX-PDF-mode t)
  (setq-default TeX-command-default "LatexMk")
  (auctex-latexmk-setup))

(provide 'module-tex)
