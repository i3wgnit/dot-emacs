;;; module-magit.el -*- lexical-binding: t; -*-

(use-package magit
  :commands
  (magit-status-internal
   magit-status-setup-buffer)
  :general
  (define-leader-key
    "g" #'magit-status)
  :init
  (setq vc-handled-backends (delq 'Git vc-handled-backends)
        magit-auto-revert-mode nil)
  :config
  (setq magit-diff-refine-hunk t))

(use-package evil-magit
  :after (evil magit)
  :demand
  :init
  (setq evil-magit-state 'normal
        evil-magit-use-z-for-folds t)
  :general
  ('(normal motion) 'magit-status-mode-map
   "<escape>" nil)
  ('(normal visual motion) 'magit-status-mode-map
   "zz" #'evil-scroll-line-to-center))

(use-package transient
  :init
  (setq transient-history-file (concat twl-local-dir "transient/history.el")
        transient-levels-file (concat twl-local-dir "transient/levels.el")
        transient-values-file (concat twl-local-dir "transient/values.el"))
  :config
  (setq transient-default-level 5)
  (twl|after magit
             (transient-append-suffix 'magit-fetch
               "-p" '("-t" "Fetch all tags" ("-t" "--tags")))))

(use-package forge
  :general
  ('(normal motion) 'forge-topic-list-mode-map
   "q" #'kill-current-buffer)
  :init
  (twl|load-after-hook magit-status forge)
  (setq forge-database-file (concat twl-local-dir "forge-database.sqlite")))

(provide 'module-magit)
