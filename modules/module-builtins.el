;;; module-builtins.el -*- lexical-binding: t; -*-

(use-package ediff
  :init
  (setq ediff-diff-options "-w"
        ediff-split-window-function #'split-window-horizontally
        ediff-window-setup-function #'ediff-setup-windows-plain)
  :config
  (defvar twl--ediff-window-configuration nil)
  (defun twl//ediff-save-window-configuration ()
    (setq tlw--ediff-window-configuration (current-window-configuration)))
  (defun twl//ediff-restore-window-confugration ()
    (set-window-configuration twl--ediff-window-configuration))

  (add-hook 'ediff-before-setup-hook #'twl//ediff-save-window-configuration)
  (add-hook 'ediff-quit-hook #'twl//ediff-restore-window-configuration 'append)
  (add-hook 'ediff-suspend-hook #'twl//ediff-restore-window-configuration
            'append))

(use-package hl-line
  :hook
  ((prog-mode text-mode conf-mode) . hl-line-mode)
  :config
  (setq hl-line-sticky-flag nil
        global-hl-line-sticky-flag nil)
  (twl|after evil
             (defvar twl+builtins--buffer-hl-line-mode nil)

             (defun twl//disable-hl-line ()
               (when hl-line-mode
                 (setq-local twl+builtins--buffer-hl-line-mode t)
                 (hl-line-mode -1)))
             (defun twl//restore-hl-line ()
               (when twl+builtins--buffer-hl-line-mode
                 (hl-line-mode +1)))
             (add-hook 'evil-visual-state-entry-hook #'twl//disable-hl-line)
             (add-hook 'evil-visual-state-exit-hook #'twl//restore-hl-line)))

(use-package paren
  :hook ((prog-mode text-mode conf-mode) . show-paren-mode)
  :config
  (setq show-paren-delay 0.02
        show-paren-highlight-openparen t
        show-paren-when-point-in-periphery t
        show-paren-when-point-inside-paren t))

(use-package recentf
  :defer 1
  :general
  (define-leader-key
    "j" #'recentf-open-files)
  ('(normal motion) 'recentf-dialog-mode-map
   "q" #'recentf-cancel-dialog)
  :init
  (twl|load-after-hook (after-find-file projectile-mode-hook) recentf)
  :config
  (setq recentf-save-file (concat twl-local-dir "recentf")
        recentf-auto-cleanup 'never
        recentf-max-menu-items 0)

  (defun twl/recentf-touch-file ()
    (when buffer-file-name
      (recentf-add-file buffer-file-name))
    nil)
  (add-hook 'write-file-functions #'twl/recentf-touch-file)

  (recentf-mode +1))

(use-package whitespace
  :delight
  :hook ((prog-mode text-mode conf-mode) . whitespace-mode)
  :config
  (setq whitespace-line-column nil
        whitespace-style
        '(empty face indentation tabs trailing lines-tail)))

(provide 'module-builtins)
