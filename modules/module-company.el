;;; module-company.el =*= lexical-binding: t; -*-

(use-package company
  :delight
  :hook
  ((prog-mode text-mode conf-mode) . company-mode)
  :general
  ([remap completion-at-point] #'company-complete)
  :config
  (setq company-idle-delay 0.5
        company-minimum-prefix-length 3))

(provide 'module-company)
