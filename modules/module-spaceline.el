;;; module-spaceline.el -*- lexical-binding: t; -*-

(use-package spaceline
  :after evil
  :defer 2
  :init
  (twl|load-after-hook window-configuration-change-hook spaceline)
  :config
  (setq-default show-help-function nil)
  (setq spaceline-highlight-face-func 'spaceline-highlight-face-evil-state))

(use-package spaceline-segments
  :ensure nil
  :after spaceline
  :demand
  :config
  (spaceline-generate main
    ;; left side
    ((evil-state
      :face highlight-face
      :priority 100)
     (anzu :priority 95)
     auto-compile
     ((buffer-modified buffer-size buffer-id remote-host)
      :priority 99)
     (major-mode :priority 79)
     ((flycheck-error flycheck-warning flycheck-info)
      :when active
      :priority 89)
     (minor-modes :when active
                  :priority 9)
     (process :when active)
     (projectile-root
      :when active
      :priority 78)
     (version-control
      :when active
      :priority 77)
     (org-clock :when active))
    ;; right side
    ((selection-info :priority 95)
     (buffer-encoding-abbrev :priority 8)
     (line-column :priority 98)
     (global :when active)
     (buffer-position :priority 97)
     (hud :priority 97)))
  (setq spaceline-byte-compile t)
  (spaceline-compile)
  (setq-default mode-line-format '("%e" (:eval (spaceline-ml-main))))

  (dolist (buffer-name '("*Messages*" "*scratch*"))
    (let ((buffer (get-buffer buffer-name)))
      (when buffer
        (with-current-buffer buffer
          (setq-local mode-line-format (default-value 'mode-line-format))
          (powerline-set-selected-window)
          (powerline-reset))))))

(use-package powerline
  :config
  (setq powerline-default-separator 'arrow)

  (defun twl+spaceline/powerline-reset (&rest _)
    (powerline-reset))
  (advice-add #'load-theme :after #'twl+spaceline/powerline-reset))

(provide 'module-spaceline)
