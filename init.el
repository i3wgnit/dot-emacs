;;; init.el -*- lexical-binding: t; -*-

;; The usual garbage collector threshold 16MB
(defvar twl-gc-cons-threshold 16777216)
(defvar twl-gc-cons-percentage 0.1)
(defun twl//defer-garbage-collection ()
  (setq gc-cons-threshold twl-gc-cons-upper-threshold
        gc-cons-percentage twl-gc-cons-upper-percentage))

;; The garbage collector limit for defering garbage collection 512MB
(defvar twl-gc-cons-upper-threshold 536870912)
(defvar twl-gc-cons-upper-percentage 0.6)
(defun twl//restore-garbage-collection ()
  (run-at-time
   1 nil
   (lambda ()
     (setq gc-cons-threshold twl-gc-cons-threshold
           gc-cons-percentage twl-gc-cons-percentage))))

(defvar twl--file-name-handler-alist file-name-handler-alist)

(defun twl//restore-startup-optimizations ()
  (setq file-name-handler-alist twl--file-name-handler-alist)
  (run-with-idle-timer
   3 nil
   (lambda ()
     (setq-default gc-cons-threshold twl-gc-cons-threshold
                   gc-cons-percentage twl-gc-cons-percentage)

     (add-hook 'minibuffer-setup-hook #'twl//defer-garbage-collection)
     (add-hook 'minibuffer-exit-hook #'twl//restore-garbage-collection)
     (add-hook 'focus-out-hook #'garbage-collect))))

(if (ignore-errors (or after-init-time noninteractive))
    (setq gc-cons-threshold twl-gc-cons-threshold
          gc-cons-percentage twl-gc-cons-percentage)
  ;; Startup optimizations
  (setq gc-cons-threshold twl-gc-cons-upper-threshold
        gc-cons-percentage twl-gc-cons-upper-percentage
        file-name-handler-alist nil)
  (add-hook 'after-init-hook #'twl//restore-startup-optimizations))

(setq user-emacs-directory
      (eval-when-compile (file-name-directory load-file-name))
      load-prefer-newer t)

(add-hook 'after-init-hook #'server-start 'append)

(require 'core (concat user-emacs-directory "core/core"))
