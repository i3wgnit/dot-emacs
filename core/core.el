;;; core.el -*- lexical-binding: t; -*-

(defvar twl-emacs-dir user-emacs-directory)
(defvar twl-core-dir (concat user-emacs-directory "core/"))

(add-to-list 'load-path twl-core-dir)
(require 'core-constants)

(defun twl//display-benchmark ()
  (message "Emacs loaded %s packages in in %.03fs seconds."
           (length package-activated-list)
           (or twl-init-time
               (setq twl-init-time
                     (float-time
                      (time-subtract (current-time)
                                     before-init-time))))))

(require 'core-lib)

(when (not (file-directory-p twl-local-dir))
  (twl/ensure-core-directories-exist))

(let (load-path)
  (add-to-list 'load-path twl-private-dir)
  (require 'local-init nil t))

(add-to-list 'load-path (concat twl-core-dir "autoload/"))
(twl/autoload "core-autoload.el")

(require 'core-packages)
(twl/initialize-packages)

(unless noninteractive
  (add-hook 'emacs-startup-hook #'twl//display-benchmark)
  (require 'core-keybinds)
  (require 'core-ui)
  (require 'core-emacs))

(require 'core-modules)

(provide 'core)
