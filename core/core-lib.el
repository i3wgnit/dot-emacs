;;; core-lib.el -*- lexical-binding: t; -*-

(require 'subr-x)
(require 'cl-lib)

(defmacro twl/log (log-string &rest args)
  `(when twl-debug-mode
     (let ((inhibit-message (active-minibuffer-window)))
       (message
        ,(concat (propertize "twl:: " 'face 'font-lock-comment-face)
                 log-string)
        ,@args))))

(defun twl/autoload (file)
  (condition-case e
      (let (command-switch-alist)
        (load (substring file 0 -3) 'noerror 'nomessage))
    ((debug error)
     (if noninteractive
         (signal 'twl-autoload-error (list (file-name-nondirectory file) e))
       (message "Autoload file warning: %s -> %s" (car e)
                (error-message-string e))))))

(defun twl/listify (exp)
  (declare (pure t))
  (if (listp exp) exp (list exp)))

(defun twl/unquote (exp)
  (declare (pure t))
  (while (memq (car-safe exp) '(quote function))
    (setq exp (cadr exp)))
  exp)

(defun twl//resolve-hook-forms (hooks)
  (declare (pure t))
  (let ((hook-list (twl/listify (twl/unquote hooks))))
    (if (eq (car-safe hooks) 'quote)
        hook-list
      (cl-loop for hook in hook-list
               if (eq (car-safe hook) 'quote)
               collect (cadr hook)
               else collect (intern (format "%s-hook" (symbol-name hook)))))))

(defmacro twl|add-transient-hook (hooks-or-functions &rest forms)
  "Attaches a self-removing function to HOOKS-OR-FUNCTIONS.
FORMS are evaluated once, when that function/hook is first invoked, then never
again.
HOOK-OR-FUNCTIONS can be a quoted hook, a sharp-quoted function (which will be
advised) or a list of either."
  (declare (indent defun) (debug t))
  (let ((append-p (when (eq (car forms) :after)
                    (pop forms)))
        (fn (if (symbolp (car forms))
                (intern (format "twl//transient-hook-%s" (pop forms)))
              (make-symbol "twl//transient-hook"))))
    (macroexp-progn
     `((fset ',fn
             (lambda (&rest _)
               ,@forms
               (dolist (sym ',(twl/listify (twl/unquote hooks-or-functions)))
                 (advice-remove sym #',fn)
                 (remove-hook sym #',fn))))
       (dolist (sym ',(twl/listify (twl/unquote hooks-or-functions)))
         (if (functionp sym)
             (advice-add sym ,(if append-p :after :before) #',fn)
           (add-hook sym #',fn ,append-p)))))))

(defmacro twl|load-after-hook (hooks-or-functions feature)
  `(twl|add-transient-hook ,hooks-or-functions :after
                           ,(make-symbol (format "-load-%s" feature))
                           (require ',feature)
                           (twl/log ,(format "Loaded %s" feature))))

(defmacro twl|add-hook (&rest args)
  "A convenience macro for adding N functions to M hooks.
If N and M = 1, there's no benefit to using this macro over `add-hook'.
This macro accepts, in order:
  1. Optional properties :local and/or :append, which will make the hook
     buffer-local or append to the list of hooks (respectively),
  2. The hook(s) to be added to: either an unquoted mode, an unquoted list of
     modes, a quoted hook variable or a quoted list of hook variables. If
     unquoted, '-hook' will be appended to each symbol.
  3. The function(s) to be added: this can be one function, a list thereof, or
     body forms (implicitly wrapped in a closure).
Examples:
    (twl|add-hook 'some-mode-hook 'enable-something)   (same as `add-hook')
    (twl|add-hook some-mode '(enable-something and-another))
    (twl|add-hook '(one-mode-hook second-mode-hook) 'enable-something)
    (twl|add-hook (one-mode second-mode) 'enable-something)
    (twl|add-hook :append (one-mode second-mode) 'enable-something)
    (twl|add-hook :local (one-mode second-mode) 'enable-something)
    (twl|add-hook (one-mode second-mode) (setq v 5) (setq a 2))
    (twl|add-hook :append :local (one-mode second-mode) (setq v 5) (setq a 2))
\(fn [:append :local] HOOKS FUNCTIONS)"
  (declare (indent defun) (debug t))
  (let ((hook-fn 'add-hook)
        append-p
        local-p)
    (while (keywordp (car args))
      (pcase (pop args)
        (:append (setq append-p t))
        (:local (setq local-p t))
        (:remove (setq hook-fn 'remove-hook))))
    (let ((hooks (twl//resolve-hook-forms (pop args)))
          (funcs
           (let ((val (car args)))
             (if (memq (car-safe val) '(quote function))
                 (if (cdr-safe (cadr val))
                     ;; list of functions
                     (cadr val)
                   ;; single function
                   (list (cadr val)))
               ;; body form
               (list args))))
          forms)
      (dolist (fn funcs)
        (setq fn (if (symbolp fn)
                     `(function ,fn)
                   `(lambda (&rest _) ,@fn)))
        (dolist (hook hooks)
          (push (if (eq hook-fn 'remove-hook)
                    `(remove-hook ',hook ,fn ,local-p)
                  `(add-hook ',hook ,fn ,append-p ,local-p))
                forms)))
      (macroexp-progn (if append-p (nreverse forms) forms)))))

(defmacro twl|remove-hook (&rest args)
  "A convenience macro for removing N functions from M hooks.
Takes the same arguments as `twl|add-hook'.
If N and M = 1, there's no benefit to using this macro over `remove-hook'.
\(fn [:append :local] HOOKS FUNCTIONS)"
  (declare (indent defun) (debug t))
  `(twl|add-hook :remove ,@args))

(defmacro twl|setq-hook (hooks &rest rest)
  "Sets buffer-local variables on HOOKS.
  (twl|setq-hook markdown-mode-hook
    line-spacing 2
    fill-column 80)
\(fn HOOKS &rest SYM VAL...)"
  (declare (indent 1))
  (unless (= 0 (% (length rest) 2))
    (signal 'wrong-number-of-arguments (list #'evenp (length rest))))
  (let ((vars (let ((args rest)
                    vars)
                (while args
                  (push (symbol-name (car args)) vars)
                  (setq args (cddr args)))
                (string-join (sort vars #'string-lessp) "-"))))
    (macroexp-progn
     (cl-loop for hook in (twl//resolve-hook-forms hooks)
              for mode = (string-remove-suffix "-hook" (symbol-name hook))
              for fn = (intern (format "twl//setq-%s-for-%s" vars mode))
              collect `(fset ',fn
                             (lambda (&rest _)
                               ,@(let (forms)
                                   (while rest
                                     (let ((var (pop rest))
                                           (val (pop rest)))
                                       (push `(setq-local ,var ,val) forms)))
                                   (nreverse forms))))
              collect `(add-hook ',hook #',fn 'append)))))

(defun twl/advice-add (symbols where functions)
  "Variadic version of `advice-add'.
SYMBOLS and FUNCTIONS can be lists of functions."
  (let ((functions (if (functionp functions)
                       (list functions)
                     functions)))
    (dolist (s (twl/listify symbols))
      (dolist (f (twl/listify functions))
        (advice-add s where f)))))

(defun twl/advice-remove (symbols where-or-fns &optional functions)
  "Variadic version of `advice-remove'.
WHERE-OR-FNS is ignored if FUNCTIONS is provided. This lets you substitute
advice-add with advice-remove and evaluate them without having to modify every
statement."
  (setq functions (or functions where-or-fns)
        where-or-fns nil)
  (let ((functions (if (functionp functions)
                       (list functions)
                     functions)))
    (dolist (s (twl/listify symbols))
      (dolist (f (twl/listify functions))
        (advice-remove s f)))))

(defmacro twl|after (targets &rest body)
  (declare (indent defun) (debug t))
  (macroexp-progn
   (if (symbolp targets)
       `((with-eval-after-load ',targets ,@body))
     (pcase (car-safe targets)
       ((or :or :any)
        (cl-loop for next in (cdr targets)
                 collect `(twl|after ,next ,@body)))
       ((or :and :all)
        (dolist (next (cdr targets))
          (setq body `((twl|after ,next ,@body))))
        body)
       (_ `((twl|after (:and ,@targets) ,@body)))))))

(defmacro twl|defer-feature (feature &optional mode)
  (let ((advice-fn (intern (format "twl//defer-feature-%s" feature)))
        (mode (or mode feature)))
    `(progn
       (setq features (delq ',feature features))
       (advice-add #',mode :before #',advice-fn)
       (defun ,advice-fn (&rest _)
         (when (and ,(intern (format "%s-hook" mode))
                    (not delay-mode-hooks))
           (provide ',feature)
           (advice-remove #',mode #',advice-fn))))))

(provide 'core-lib)
