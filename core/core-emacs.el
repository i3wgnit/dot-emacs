;;; core-emacs.el -*- lexical-binding: t; -*-

(set-charset-priority 'unicode)
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq locale-coding-system 'utf-8)
(setq-default buffer-file-coding-system 'utf-8)
(unless IS-WINDOWS
  (setq-default selection-coding-system 'utf-8))

(setq-default
 apropos-do-all t
 auto-mode-case-fold nil
 autoload-compute-prefixes nil
 ffap-machine-known-p 'reject
 find-file-visit-truename t
 idle-update-delay 1
 inhibit-startup-echo-area-message user-login-name
 inhibit-startup-message t
 initial-major-mode 'fundamental-mode
 initial-scratch-message nil
 large-file-warning-threshold 16777216

 auto-save-default t
 backup-by-copying t
 create-lockfiles nil
 delete-old-versions t
 make-backup-file t
 vc-follow-symlinks t
 version-control t

 byte-compile-verbose t
 byte-compile-warnings '(not free-vars unresolved noruntime lexical make-local)

 abbrev-file-name (concat twl-local-dir "abbrev.el")
 auto-save-list-file-name (concat twl-local-dir "autosave")
 backup-directory-alist (list (cons "." twl-backup-dir))
 bookmark-default-file (concat twl-local-dir "bookmarks")
 custom-file (concat twl-private-dir "custom.el")
 custom-theme-directory (concat twl-private-dir "themes/")

 delete-trailing-lines t
 fill-column 80
 sentence-end-double-space t
 word-wrap t

 hscroll-margin 2
 hscroll-step 1
 mouse-wheel-progressive-speed nil
 mouse-wheel-scroll-amount '(5 ((shift) . 2))
 scroll-conservatively 1001
 scroll-margin 3
 scroll-preserve-screen-position t

 indent-tabs-mode nil
 require-final-newline t
 tab-always-indent t
 tab-always-indent t
 tab-width 4
 tabify-regex "^\t* [ \t]+")

(twl|setq-hook (eshell-mode-hook term-mode-hook) hscroll-margin 0)

(provide 'core-emacs)
