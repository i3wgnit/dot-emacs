;;; core-modules.el -*- lexical-binding: t; -*-

(use-package exec-path-from-shell
  :when (or IS-LINUX IS-MAC)
  :demand
  :config
  (exec-path-from-shell-initialize))
(use-package use-package-ensure-system-package)
(use-package delight)

(add-to-list 'load-path twl-modules-dir)

;; Important modules
(require 'module-evil)
(require 'module-builtins)

;; Functionality modules
(require 'module-magit)
(require 'module-projectile)
(require 'module-yasnippet)

;; UI modules
(require 'module-company)
(require 'module-ivy)
(require 'module-spaceline)
(require 'module-which-key)

;; Major mode modules
(require 'module-tex)

(provide 'core-modules)
