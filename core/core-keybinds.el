;;; core-keybinds.el -*- lexical-binding: t; -*-

;;
;;; Variables

(defvar twl-leader-map (make-sparse-keymap))
(defvar twl-leader ",")
(defvar twl-leader-alt "M-,")
(defvar twl-localleader "\\")
(defvar twl-localleader-alt "M-\\")

(defvar twl-escape-hook nil)

;;
;;; Miscellaneous

(use-package general
  :demand
  :config
  (general-auto-unbind-keys))

(defun twl/escape ()
  (interactive)
  (cond
   ((minibuffer-window-active-p (minibuffer-window))
    (abort-recursive-edit))
   ((run-hook-with-args-until-success 'twl-escape-hook))
   ((or defining-kbd-macro executing-kbd-macro) nil)
   ((keyboard-quit))))

(general-def [remap keyboard-quit] #'twl/escape)

(general-create-definer define-leader-key
  :states '(normal visual motion emacs insert)
  :prefix twl-leader
  :non-normal-prefix twl-leader-alt
  :prefix-map 'twl-leader-map)

(general-create-definer define-localleader-key
  :states '(normal visual motion emacs insert)
  :major-modes t
  :prefix twl-localleader
  :non-normal-prefix twl-localleader-alt
  :prefix-map 'twl-leader-map)

(defun twl/config-visit ()
  (interactive)
  (find-file (concat twl-emacs-dir "init.el")))

(defun twl/config-reload ()
  (interactive)
  (load (concat twl-emacs-dir "init")))

(general-def
  :prefix "C-c"
  "e" #'twl/config-visit
  "r" #'twl/config-reload)

(provide 'core-keybinds)
