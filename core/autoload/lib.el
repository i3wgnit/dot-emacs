;;; lib.el -*- lexical-binding: t; -*-

;;;###autoload
(defun twl/insert-date ()
  (interactive)
  (insert (format-time-string "%B %d, %Y" (current-time))))
