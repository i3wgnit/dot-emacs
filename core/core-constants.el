;;; core-constants.el -*- lexical-binding: t; -*-

(defconst EMACS26+ (> emacs-major-version 25))
(defconst EMACS27+ (> emacs-major-version 26))

(defconst IS-MAC     (eq system-type 'darwin))
(defconst IS-LINUX   (eq system-type 'gnu/linux))
(defconst IS-WINDOWS (memq system-type '(cygwin windows-nt ms-dos)))
(defconst IS-BSD     (or IS-MAC (eq system-type 'berkeley-unix)))

(defvar twl-init-time nil)
(defvar twl-debug-mode init-file-debug)
(defvar twl--package-refreshed-p nil
  "Non-nil if packages have been refreshed during initialization.")

(defvar twl-local-dir (concat twl-emacs-dir ".local/")
  "The root directory for local storage.")
(defvar twl-modules-dir (concat twl-emacs-dir "modules/")
  "Where core-modules load files from. Must end with a slash.")
(defvar twl-private-dir (concat twl-emacs-dir "private/"))
(defvar twl-backup-dir (concat twl-local-dir "backups/"))
(defvar twl-package-dir (concat twl-local-dir "packages/"))

(defun twl/ensure-core-directories-exist ()
  (dolist (dir (list twl-local-dir
                     twl-private-dir
                     twl-backup-dir))
    (unless (file-directory-p dir)
      (make-directory dir 'parents))))

(provide 'core-constants)
