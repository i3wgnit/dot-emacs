;;; core-packages.el -*- lexical-binding: t; -*-

(setq package-user-dir twl-package-dir
      package-enable-at-startup nil)
(require 'package)
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
(setq load-path (cl-delete-if-not #'file-directory-p load-path))

(defun twl//install-package (pkg)
  (unless (package-installed-p pkg)
    (unless twl--package-refreshed-p
      (package-refresh-contents)
      (setq twl--package-refreshed-p t))
    (package-install pkg)))

(defun twl/initialize-packages ()
  (twl//install-package 'use-package)
  (eval-when-compile
    (require 'use-package))
  (setq use-package-always-defer t
        use-package-always-ensure t
        use-package-verbose twl-debug-mode))

(provide 'core-packages)
