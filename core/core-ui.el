;;; core-ui.el -*- lexical-binding: t; -*-

;;
;;; Variables

(defvar twl-theme 'doom-vibrant)
(defvar twl-font (if IS-MAC "Iosevka-18" "Iosevka-14"))

;;
;;; Miscellaneous

(defun twl//disable-all-themes (&rest _)
  (dolist (theme custom-enabled-themes)
    (disable-theme theme)))

;;
;;; Initializers

(defun twl//initialize-fonts ()
  (condition-case e
      (cond
       (twl-font (add-to-list
                  'default-frame-alist
                  (cons 'font
                        (cond
                         ((stringp twl-font) twl-font)
                         ((fontp twl-font) (font-xlfd-name twl-font))
                         ((signal 'wrong-type-argument
                                  (list '(fontp stringp) twl-font)))))))
       ((display-graphic-p)
        (setq twl-font (face-attribute 'default :font))))
    ((debug error)
     (if (string-prefix-p "Font not available: " (error-message-string e))
         (lwarn 'core-ui :warning
                "Could not find the %s font on your system, falling back to
 system font"
                (font-get (caddr e) :family)))
     (signal 'core-error e))))

(defun twl//initialize-theme (&optional frame)
  (when (and twl-theme
             (not (memq twl-theme custom-enabled-themes)))
    (with-selected-frame (or frame (selected-frame))
      (load-theme twl-theme t))))

(defun twl//initialize-ui ()
  (twl//initialize-theme)
  (advice-add #'load-theme :before #'twl//disable-all-themes)
  (window-divider-mode))

(add-hook (if (daemonp)
              'after-make-frame-functions
            'window-setup-hook)
          #'twl//initialize-ui)
(twl//initialize-fonts)

;;
;;; Default settings

(setq-default
 ansi-color-for-comint-mode t
 bidi-display-reordering nil
 blink-matching-parent nil
 compilation-always-kill t
 compilation-ask-about-save t
 compilation-scroll-output 'first-output
 confirm-kill-emacs nil
 confirm-nonexistent-file-or-buffer 'after-completion
 cursor-in-non-selected-windows nil
 echo-keystrokes 0.02
 enable-recursive-minibuffers nil
 frame-inhibit-implied-resize t
 highlight-nonselected-windows nil
 image-animate-loop t
 indicate-buffer-boundaries nil
 indicate-empty-lines t
 max-mini-window-height 0.3
 mode-line-default-help-echo nil
 mouse-yank-at-point t
 show-help-function nil
 uniquify-buffer-name-style 'reverse
 use-dialog-box nil
 visible-cursor nil
 x-stretch-cursor nil

 display-line-numbers-width 3
 display-line-numbers-type 'visual
 display-line-numbers 'visual

 split-width-threshold 160
 split-height-threshold nil

 ring-bell-function #'ignore
 visual-bell nil

 window-resize-pixelwise t
 frame-resize-pixelwise t

 show-trailing-whitespace-mode nil

 window-divider-default-places t
 window-divider-default-bottom-width 1
 window-divider-default-right-width 1)

(fset 'yes-or-no-p 'y-or-n-p)
(fset #'display-startup-echo-area-message #'ignore)

(if (bound-and-true-p tooltip-mode) (tooltip-mode -1))
(blink-cursor-mode -1)

(add-to-list 'default-frame-alist '(tool-bar-lines . 0))
(add-to-list 'default-frame-alist '(menu-bar-lines . 0))
(add-to-list 'default-frame-alist '(vertical-scroll-bars))
(setq scroll-bar-mode nil)

(setq resize-mini-windows t)
(twl|setq-hook minibuffer-setup-hook
               resize-mini-windows 'grow-only)

;; This is faster than whitespace-mode, but I prefer whitespace-mode
;; (twl|setq-hook (prog-mode-hook text-mode-hook conf-mode-hook)
;;   show-trailing-whitespace t)

(setq minibuffer-prompt-properties
      '(read-only t intangible t cursor-intangible t face minibuffer-prompt))
(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

;;
;;; Packages

(use-package doom-themes
  :hook (org-mode . doom-themes-org-config)
  :init
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))


(provide 'core-ui)
